const xhr = new XMLHttpRequest();
xhr.open('Get', 'http://13.90.159.78/compositions');
xhr.setRequestHeader('Authorization', 'general-auth-token');
xhr.onreadystatechange = function() {
    if (xhr.readyState == 4) {
        const compositions = JSON.parse(xhr.responseText);
        for (let composition of compositions) {
            function addRow() {
                let table = document.getElementById('compositions-table');
                let row = table.insertRow(-1);

                let titleCell = row.insertCell(0);
                let title = document.createTextNode(composition.title);
                titleCell.appendChild(title);

                let yearCell = row.insertCell(1);
                let year = document.createTextNode(composition.yearComposed);
                yearCell.appendChild(year);

                let genreCell = row.insertCell(2);
                let genre = document.createTextNode(composition.genre);
                genreCell.appendChild(genre);

                let multiMovementCell = row.insertCell(3);
                let multiMovement = document.createTextNode(composition.multiMovement);
                multiMovementCell.appendChild(multiMovement);

                let composerCell = row.insertCell(4);
                let composer = document.createTextNode(composition.composer.name);
                composerCell.appendChild(composer);
            }
            addRow();
        };
    }
}
xhr.send();