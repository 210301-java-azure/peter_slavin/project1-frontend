const xhr = new XMLHttpRequest();
xhr.open('Get', 'http://13.90.159.78/composers');
xhr.setRequestHeader('Authorization', 'general-auth-token');
xhr.onreadystatechange = function() {
    if (xhr.readyState == 4) {
        var composers = JSON.parse(xhr.responseText);
        for (let composer of composers) {
            function addRow() {
                let table = document.getElementById('composers-table');
                let row = table.insertRow(-1);

                let nameCell = row.insertCell(0);
                let name = document.createTextNode(composer.name);
                nameCell.appendChild(name);

                let birthYearCell = row.insertCell(1);
                let birthYear = document.createTextNode(composer.birthYear);
                birthYearCell.appendChild(birthYear);

                let deathYearCell = row.insertCell(2);
                let deathYear = composer.deathYear > 0 ? composer.deathYear : '';
                deathYearOrNull = document.createTextNode(deathYear);
                deathYearCell.appendChild(deathYearOrNull);
            }
            addRow();
        };
    }
}
xhr.send();