document.getElementById('add-new-composition').addEventListener('submit', addNewComposition);

// populate composer select
const xhr = new XMLHttpRequest();
xhr.open('Get', 'http://13.90.159.78/composers');
xhr.setRequestHeader('Authorization', 'general-auth-token');
xhr.onreadystatechange = function() {
    if (xhr.readyState == 4) {
        const composers = JSON.parse(xhr.responseText);
        for (composer of composers) {
            let option = document.createElement('option');
            option.innerText = composer.name;
            option.value = composer.id;
            option.composer = composer;
            document.getElementById('composer').appendChild(option);
        }
    }
}
xhr.send();

function addNewComposition(e) {
    e.preventDefault();
    let title = document.getElementById('title').value;
    let yearComposed = document.getElementById('year-composed').value;
    let genre = document.getElementById('genre').value;
    let multiMovement = document.getElementById('multi-true').checked ? true : false;
    console.log(document.getElementById('multi-true').checked);
    let compId = document.getElementById('composer').value;
    let composer = {id: compId}
    let newComposition = {'title': title, 'yearComposed': yearComposed, 'genre': genre, 'multiMovement': multiMovement, 'composer': composer};
    addComposition(newComposition, success, failure);
}

function success() {
    const message = document.getElementById('create-msg'); 
    message.hidden = false;
    message.innerText = 'New composition successfully created';
}

function failure() {
    const message = document.getElementById('create-msg'); 
    message.hidden = false;
    message.innerText = 'Unable to add the new composition';
}