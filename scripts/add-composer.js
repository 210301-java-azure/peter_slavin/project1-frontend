document.getElementById('add-new-composer').addEventListener('submit', addNewComposer);

function addNewComposer(e) {
    e.preventDefault();
    const name = document.getElementById('name').value;
    const birthYear = document.getElementById('birth-year').value;
    let deathYear = document.getElementById('death-year').value;
    const newComposer = {'name': name, 'birthYear': birthYear, 'deathYear': deathYear};
    addComposer(newComposer, success, failure);
}

function success() {
    const message = document.getElementById('create-msg'); 
    message.hidden = false;
    message.innerText = 'New composer successfully created';
}

function failure() {
    const message = document.getElementById('create-msg'); 
    message.hidden = false;
    message.innerText = 'Unable to add the new composer';
}