document.getElementById('user-by-username').addEventListener('submit', getUserByUserName);

const xhr = new XMLHttpRequest();
xhr.open('Get', 'http://13.90.159.78/users');
xhr.setRequestHeader('Authorization', 'admin-auth-token');
xhr.onreadystatechange = function() {
    if (xhr.readyState == 4) {
        var users = JSON.parse(xhr.responseText);
        for (let user of users) {
            function addRow() {
                let table = document.getElementById('users-table');
                let row = table.insertRow(-1);

                let userNameCell = row.insertCell(0);
                let userName = document.createTextNode(user.userName);
                userNameCell.appendChild(userName);
            }
            addRow();
        };
    }
}
xhr.send();

function getUserByUserName(e) {
    e.preventDefault();
    const userName = document.getElementById('userName').value;
    const xhr = new XMLHttpRequest(); 
    xhr.open('GET', `http://13.90.159.78/users?username=${userName}`);
    xhr.setRequestHeader('Authorization', 'admin-auth-token');
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
            if (xhr.status > 199 && xhr.status < 300) {
                displayUserData(xhr.response);
            } else {
                const error = document.createElement('li');
                error.innerText = ('Unable to retrieve user info')
                document.getElementById('user-data').appendChild(error);
            }
        }
    }
    xhr.send();
}

function displayUserData(data) {
    const userData = JSON.parse(data)
    for (const [key, value] of Object.entries(userData)) {
        let list = document.getElementById('user-data');
        if (key != 'password') {
            let listItem = document.createElement('li');
            listItem.innerText = `${key}: ${value}`;
            list.appendChild(listItem);
        }
    }
}