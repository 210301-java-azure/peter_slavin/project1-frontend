document.getElementById('login-form').addEventListener('submit', login);

function login(e) {
    e.preventDefault();
    const userName = document.getElementById('userName').value;
    const password = document.getElementById('password').value;
    loginRequest(userName, password, success, failure)
}

function success(xhr) {
    const authToken = xhr.getResponseHeader('Authorization');
    sessionStorage.setItem('authToken', authToken);
    if (authToken) {
        window.location.href = './compositions.html';
    } else {
        document.getElementById('login-form').reset();
    }
}

function failure(xhr) {
    const error = document.getElementById('error-msg');
    error.hidden = false;
    error.innerText = xhr.responseText;
}