document.getElementById('delete-composition').addEventListener('click', deleteComposition);

const xhr = new XMLHttpRequest();
xhr.open('Get', 'http://13.90.159.78/compositions');
xhr.setRequestHeader('Authorization', 'general-auth-token');
xhr.onreadystatechange = function() {
    if (xhr.readyState == 4) {
        const compositions = JSON.parse(xhr.responseText);
        for (composition of compositions) {
            let option = document.createElement('option');
            option.innerText = composition.title;
            option.value = composition.id;
            option.composition = composition;
            document.getElementById('composition').appendChild(option);
        }
    }
}
xhr.send();

function deleteComposition(e){
    e.preventDefault();
    let compositionId = document.getElementById('composition').value;
;    console.log(composition)
    deleteComposition(compositionId, success, failure);
}

function success(){
    const message = document.getElementById('create-msg'); 
    message.hidden = false;
    message.innerText = 'Composition deleted';
}

function failure(){
    const message = document.getElementById('create-msg'); 
    message.hidden = false;
    message.innerText = 'Unable to delete the composition.';
}