
function sendRequest(method, url, body, success, failure, authToken) {
    const xhr = new XMLHttpRequest(); 
    xhr.open(method, url);
    if (authToken){
        xhr.setRequestHeader('Authorization', authToken);
    }
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
            if (xhr.status > 199 && xhr.status < 300) {
                success(xhr);
            } else {
                failure(xhr);
            }
        }
    }
    if(body) {
        xhr.send(body);
    } else {
        xhr.send();
    }
}

function postRequest(url, body, success, failure, authToken) {
    sendRequest('POST', url, body, success, failure, authToken);
}

function deleteRequest(url, success, failure, authToken) {
    sendRequest('DELETE', url, null, success, failure, authToken);
}

function loginRequest(userName, password, success, failure) {
    const payload = `userName=${userName}&password=${password}`;
    postRequest('http://13.90.159.78/login', payload, success, failure);
}

function addComposer(composer, success, failure) {
    const composerJSON = JSON.stringify(composer);
    const token = sessionStorage.getItem('authToken');
    postRequest('http://13.90.159.78/composers', composerJSON, success, failure, token);
}

function addComposition(composition, success, failure) {
    const compositionJSON = JSON.stringify(composition);
    const token = sessionStorage.getItem('authToken');
    postRequest('http://13.90.159.78/compositions', compositionJSON, success, failure, token);
}

function deleteComposition(id, success, failure) {
    const token = sessionStorage.getItem('authToken');
    deleteRequest(`http://13.90.159.78/compositions/${id}`, success, failure, token);
}
 
